﻿namespace Egraph.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.LabelCorr = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.графЗадачіToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.графКСToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vоделюванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.параметриПроцесорівToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.числоФізичнихЛінківToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.наявністьПроцесораВводувиводуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дуплексністьЗвязківToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.спосібПересилкиДанихToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пересилкаПовідомленьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.конвейеризаціяПересилокПакетамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.діаграмаГантаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.методФормуванняЧергToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.алгоритмПризначенняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.параметриToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.результатиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.допомогаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вихідToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(550, 551);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Граф задачі";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(35, 20);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 6;
            this.button9.Text = "add edge";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(7, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(537, 496);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(359, 20);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "auto";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(278, 20);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "rm link";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(197, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "add link";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(116, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "rm edge";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // LabelCorr
            // 
            this.LabelCorr.AutoSize = true;
            this.LabelCorr.Location = new System.Drawing.Point(16, 588);
            this.LabelCorr.Name = "LabelCorr";
            this.LabelCorr.Size = new System.Drawing.Size(25, 13);
            this.LabelCorr.TabIndex = 7;
            this.LabelCorr.Text = "corr";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.button12);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Location = new System.Drawing.Point(568, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(550, 551);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Граф КС";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(448, 26);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(79, 17);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "связность";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(367, 20);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 6;
            this.button12.Text = "auto";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(285, 19);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 5;
            this.button11.Text = "rm link";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(203, 19);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 4;
            this.button10.Text = "add link";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(121, 19);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 3;
            this.button8.Text = "rm edge";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(39, 19);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 2;
            this.button7.Text = "add edge";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(7, 49);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(537, 496);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.графЗадачіToolStripMenuItem,
            this.графКСToolStripMenuItem,
            this.vоделюванняToolStripMenuItem,
            this.статистикаToolStripMenuItem,
            this.допомогаToolStripMenuItem,
            this.вихідToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1136, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // графЗадачіToolStripMenuItem
            // 
            this.графЗадачіToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.generateToolStripMenuItem});
            this.графЗадачіToolStripMenuItem.Name = "графЗадачіToolStripMenuItem";
            this.графЗадачіToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.графЗадачіToolStripMenuItem.Text = "Граф задачі";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.newToolStripMenuItem.Text = "new";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.loadToolStripMenuItem.Text = "load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.saveToolStripMenuItem.Text = "save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.generateToolStripMenuItem.Text = "generate";
            this.generateToolStripMenuItem.Click += new System.EventHandler(this.generateToolStripMenuItem_Click);
            // 
            // графКСToolStripMenuItem
            // 
            this.графКСToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem1,
            this.loadToolStripMenuItem1,
            this.saveToolStripMenuItem1});
            this.графКСToolStripMenuItem.Name = "графКСToolStripMenuItem";
            this.графКСToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.графКСToolStripMenuItem.Text = "Граф КС";
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(97, 22);
            this.newToolStripMenuItem1.Text = "new";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripMenuItem1_Click);
            // 
            // loadToolStripMenuItem1
            // 
            this.loadToolStripMenuItem1.Name = "loadToolStripMenuItem1";
            this.loadToolStripMenuItem1.Size = new System.Drawing.Size(97, 22);
            this.loadToolStripMenuItem1.Text = "load";
            this.loadToolStripMenuItem1.Click += new System.EventHandler(this.loadToolStripMenuItem1_Click);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(97, 22);
            this.saveToolStripMenuItem1.Text = "save";
            this.saveToolStripMenuItem1.Click += new System.EventHandler(this.saveToolStripMenuItem1_Click);
            // 
            // vоделюванняToolStripMenuItem
            // 
            this.vоделюванняToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.параметриПроцесорівToolStripMenuItem,
            this.діаграмаГантаToolStripMenuItem,
            this.статистикаToolStripMenuItem1,
            this.методФормуванняЧергToolStripMenuItem,
            this.алгоритмПризначенняToolStripMenuItem});
            this.vоделюванняToolStripMenuItem.Name = "vоделюванняToolStripMenuItem";
            this.vоделюванняToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.vоделюванняToolStripMenuItem.Text = "Моделювання";
            // 
            // параметриПроцесорівToolStripMenuItem
            // 
            this.параметриПроцесорівToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.числоФізичнихЛінківToolStripMenuItem,
            this.наявністьПроцесораВводувиводуToolStripMenuItem,
            this.дуплексністьЗвязківToolStripMenuItem,
            this.спосібПересилкиДанихToolStripMenuItem});
            this.параметриПроцесорівToolStripMenuItem.Name = "параметриПроцесорівToolStripMenuItem";
            this.параметриПроцесорівToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.параметриПроцесорівToolStripMenuItem.Text = "параметри процесорів";
            // 
            // числоФізичнихЛінківToolStripMenuItem
            // 
            this.числоФізичнихЛінківToolStripMenuItem.Name = "числоФізичнихЛінківToolStripMenuItem";
            this.числоФізичнихЛінківToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.числоФізичнихЛінківToolStripMenuItem.Text = "число фізичних лінків";
            // 
            // наявністьПроцесораВводувиводуToolStripMenuItem
            // 
            this.наявністьПроцесораВводувиводуToolStripMenuItem.Name = "наявністьПроцесораВводувиводуToolStripMenuItem";
            this.наявністьПроцесораВводувиводуToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.наявністьПроцесораВводувиводуToolStripMenuItem.Text = "наявність процесора вводу-виводу";
            // 
            // дуплексністьЗвязківToolStripMenuItem
            // 
            this.дуплексністьЗвязківToolStripMenuItem.Name = "дуплексністьЗвязківToolStripMenuItem";
            this.дуплексністьЗвязківToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.дуплексністьЗвязківToolStripMenuItem.Text = "дуплексність зв’язків";
            // 
            // спосібПересилкиДанихToolStripMenuItem
            // 
            this.спосібПересилкиДанихToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пересилкаПовідомленьToolStripMenuItem,
            this.конвейеризаціяПересилокПакетамиToolStripMenuItem});
            this.спосібПересилкиДанихToolStripMenuItem.Name = "спосібПересилкиДанихToolStripMenuItem";
            this.спосібПересилкиДанихToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.спосібПересилкиДанихToolStripMenuItem.Text = "спосіб пересилки даних";
            // 
            // пересилкаПовідомленьToolStripMenuItem
            // 
            this.пересилкаПовідомленьToolStripMenuItem.Name = "пересилкаПовідомленьToolStripMenuItem";
            this.пересилкаПовідомленьToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.пересилкаПовідомленьToolStripMenuItem.Text = "пересилка повідомлень";
            // 
            // конвейеризаціяПересилокПакетамиToolStripMenuItem
            // 
            this.конвейеризаціяПересилокПакетамиToolStripMenuItem.Name = "конвейеризаціяПересилокПакетамиToolStripMenuItem";
            this.конвейеризаціяПересилокПакетамиToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.конвейеризаціяПересилокПакетамиToolStripMenuItem.Text = "конвейеризація пересилок пакетами";
            // 
            // діаграмаГантаToolStripMenuItem
            // 
            this.діаграмаГантаToolStripMenuItem.Name = "діаграмаГантаToolStripMenuItem";
            this.діаграмаГантаToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.діаграмаГантаToolStripMenuItem.Text = "діаграма Ганта";
            // 
            // статистикаToolStripMenuItem1
            // 
            this.статистикаToolStripMenuItem1.Name = "статистикаToolStripMenuItem1";
            this.статистикаToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.статистикаToolStripMenuItem1.Text = "статистика";
            // 
            // методФормуванняЧергToolStripMenuItem
            // 
            this.методФормуванняЧергToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.методФормуванняЧергToolStripMenuItem.Name = "методФормуванняЧергToolStripMenuItem";
            this.методФормуванняЧергToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.методФормуванняЧергToolStripMenuItem.Text = "Метод формування черг";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem2.Text = "3";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem3.Text = "9";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(86, 22);
            this.toolStripMenuItem4.Text = "14";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // алгоритмПризначенняToolStripMenuItem
            // 
            this.алгоритмПризначенняToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.алгоритмПризначенняToolStripMenuItem.Name = "алгоритмПризначенняToolStripMenuItem";
            this.алгоритмПризначенняToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.алгоритмПризначенняToolStripMenuItem.Text = "Алгоритм призначення";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem5.Text = "2";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem6.Text = "5";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // статистикаToolStripMenuItem
            // 
            this.статистикаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.параметриToolStripMenuItem,
            this.результатиToolStripMenuItem});
            this.статистикаToolStripMenuItem.Name = "статистикаToolStripMenuItem";
            this.статистикаToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.статистикаToolStripMenuItem.Text = "Статистика";
            // 
            // параметриToolStripMenuItem
            // 
            this.параметриToolStripMenuItem.Name = "параметриToolStripMenuItem";
            this.параметриToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.параметриToolStripMenuItem.Text = "параметри ";
            // 
            // результатиToolStripMenuItem
            // 
            this.результатиToolStripMenuItem.Name = "результатиToolStripMenuItem";
            this.результатиToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.результатиToolStripMenuItem.Text = "результати";
            // 
            // допомогаToolStripMenuItem
            // 
            this.допомогаToolStripMenuItem.Name = "допомогаToolStripMenuItem";
            this.допомогаToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.допомогаToolStripMenuItem.Text = "Допомога";
            this.допомогаToolStripMenuItem.Click += new System.EventHandler(this.допомогаToolStripMenuItem_Click);
            // 
            // вихідToolStripMenuItem
            // 
            this.вихідToolStripMenuItem.Name = "вихідToolStripMenuItem";
            this.вихідToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.вихідToolStripMenuItem.Text = "Вихід";
            this.вихідToolStripMenuItem.Click += new System.EventHandler(this.вихідToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 610);
            this.Controls.Add(this.LabelCorr);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem графЗадачіToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem графКСToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vоделюванняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem параметриПроцесорівToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem числоФізичнихЛінківToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наявністьПроцесораВводувиводуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дуплексністьЗвязківToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem спосібПересилкиДанихToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пересилкаПовідомленьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem конвейеризаціяПересилокПакетамиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem діаграмаГантаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem статистикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem параметриToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem результатиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem допомогаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вихідToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методФормуванняЧергToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolStripMenuItem алгоритмПризначенняToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        public System.Windows.Forms.Label LabelCorr;
    }
}