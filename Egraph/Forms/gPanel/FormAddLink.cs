﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms.gPanel
{
    public partial class FormAddLink : Form
    {
        private DrawingGraph drawingGraph;

        public FormAddLink(DrawingGraph drawingGraph)
        {
            InitializeComponent();
            this.drawingGraph = drawingGraph;

        }

        private void FormAddLink_Load(object sender, EventArgs e)
        {
            if(drawingGraph.getEdges().Length < 1)
                return;

            comboBox1.Items.AddRange(drawingGraph.getEdgesNames());
            comboBox1.SelectedItem = drawingGraph.getEdges()[0]+"";
            comboBox2.Items.AddRange(drawingGraph.getEdgesNames());
            comboBox2.SelectedItem = drawingGraph.getEdges()[0]+"";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text.Length<1 || comboBox1.Text.Length < 1 || comboBox2.Text.Length < 1 || comboBox1.Text.Equals(comboBox2.Text))
                return;
            if(int.Parse(comboBox1.Text) > int.Parse(comboBox2.Text))
            {
                return;
            }
            drawingGraph.addLink(new Link(textBox1.Text, comboBox1.Text, comboBox2.Text));
            this.Hide();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
       
                textBox1.Text = "1";
            
        }
    }
}
