﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms.gPanel
{
    public partial class FormAddEdge : Form
    {
        private DrawingGraph drawingGraph;

        public FormAddEdge(DrawingGraph drawingGraph)
        {
            InitializeComponent();
            this.drawingGraph = drawingGraph;
            textBox1.Text = (this.drawingGraph.edges.Count + 1)+"";
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text.Equals("")||textBox2.Text.Equals(""))
                return;

            drawingGraph.addEdge(new Edge(textBox1.Text, textBox2.Text));
            this.Hide();
        }

        private void FormAddEdge_Load(object sender, EventArgs e)
        {
            this.ActiveControl = textBox2;
        }
    }
}
