﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms.gPanel
{
    public partial class FormRmLink : Form
    {
        private DrawingGraph drawingGraph;

        public FormRmLink(DrawingGraph drawingGraph)
        {
            InitializeComponent();
            this.drawingGraph = drawingGraph;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem.ToString().Length<1)
                return;
            drawingGraph.rmLink(comboBox1.SelectedItem.ToString());
            this.Hide();
        }

        private void FormRmLink_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(drawingGraph.getLinks());
        }
    }
}
