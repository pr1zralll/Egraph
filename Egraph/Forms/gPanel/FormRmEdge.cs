﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms.gPanel
{
    public partial class FormRmEdge : Form
    {
        private DrawingGraph drawingGraph;

        public FormRmEdge(DrawingGraph taskDrawingGraph)
        {
            InitializeComponent();
            this.drawingGraph = taskDrawingGraph;
        }

        private void FormRmEdge_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(drawingGraph.getEdgesNames());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem.ToString().Length<1)
                return;
            drawingGraph.rmEdge(comboBox1.SelectedItem.ToString());
            this.Hide();
        }
    }
}
