﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms
{
    public partial class FormGen : Form
    {
        private TaskGraph tGraph;

        public FormGen(TaskGraph taskGraph)
        {
            InitializeComponent();
            this.tGraph = taskGraph;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int max_weight = 0;
            int min_weight=0;
            int edge_count=0;
            float corr=0;
            int min_link=0;
            int max_link=0;
            try
            {
                 min_weight = Convert.ToInt32(textBox1.Text);
                 max_weight = Convert.ToInt32(textBox2.Text);
                 edge_count = Convert.ToInt32(textBox3.Text);
                 corr = Convert.ToSingle(textBox4.Text);
                 min_link = Convert.ToInt32(textBox5.Text);
                 max_link = Convert.ToInt32(textBox6.Text);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            tGraph.gGen(min_weight, max_weight, edge_count, corr, min_link, max_link);
            this.Hide();

        }
    }
}
