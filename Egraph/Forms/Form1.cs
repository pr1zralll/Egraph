﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph.Forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            _taskGraph = new TaskGraph();
            _systemGraph = new SystemGraph();
            model = new Modeling(_taskGraph,_systemGraph);
            
            InitializeComponent();
            stateBox = checkBox1;
            corr = LabelCorr;
        }

        public static CheckBox stateBox;
        public static Label corr;
        private TaskGraph _taskGraph;
        private SystemGraph _systemGraph;

        private void Form1_Load(object sender, EventArgs e)
        {
            button12_Click(null,null);
            button5_Click(null, null);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            pictureBox1.Image = _taskGraph.drawGraph();
            pictureBox2.Image = _systemGraph.drawGraph();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            _taskGraph.gAddEdge();
            this.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _taskGraph.gRmEdge();
            this.Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _taskGraph.gAddLink();
            this.Invalidate();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            _taskGraph.gRmLink();
            this.Invalidate();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            _taskGraph.load(@"C:\Users\Pavel\Documents\1.tg");
            this.Invalidate();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _taskGraph.onLoad(DrawingGraph.FileFormat.TaskGraph);
            this.Invalidate();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _taskGraph.onSave(DrawingGraph.FileFormat.TaskGraph);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _taskGraph.Clear();
            this.Invalidate();
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _systemGraph.Clear();
            this.Invalidate();
        }

        private void loadToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _systemGraph.onLoad(DrawingGraph.FileFormat.SystemGraph);
            this.Invalidate();
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            _systemGraph.onSave(DrawingGraph.FileFormat.SystemGraph);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            _systemGraph.gAddEdge();
            this.Invalidate();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            _systemGraph.gRmEdge();
            this.Invalidate();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            _systemGraph.gAddLink();
            this.Invalidate();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            _systemGraph.gRmLink();
            this.Invalidate();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            _systemGraph.load(@"C:\Users\Pavel\Documents\test.sg");
            this.Invalidate();
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void допомогаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pavel Khilinskyi \n 2017");
        }

        private Modeling model;
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            model.queue_3();
            this.Invalidate();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            model.queue_14();
            this.Invalidate();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            model.queue_9();
            this.Invalidate();
        }

        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGen formGen = new FormGen(_taskGraph);
            formGen.ShowDialog();
            this.Invalidate();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                MessageBox.Show("граф КС не звязний");
                return;
            }
            model.process_2();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                MessageBox.Show("граф КС не звязний");
                return;
            }
            model.process_5();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
