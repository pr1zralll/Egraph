﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egraph
{
    public class BaseGraph
    {
        public List<Edge> edges;
        public List<Link> links;

        public BaseGraph()
        {
            edges = new List<Edge>();
            links = new List<Link>();
        }
        public void RevertLinks()
        {
            for (int i = 0; i < links.Count; i++)
            {
                links[i].Revert();
            }
        }

        public void addEdge(Edge e)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                if (edges[i].id==e.id)
                {
                    edges.Remove(edges[i]);
                }
            }
            edges.Add(e);
        }

        public void addLink(Link l)
        {
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].from == l.from && links[i].to == l.to)
                {
                    links.Remove(links[i]);
                }
            }
            links.Add(l);
        }
        public void addLinkSum(Link l)
        {
            int temp = 0;
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].name.Equals(l.name))
                {
                    
                    temp = links[i].weight;
                    
                        links.Remove(links[i]);
                }
            }
            l.weight = l.weight + temp;
            links.Add(l);
        }
        public void rmEdge(string id)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                if (edges[i].id==Int32.Parse(id))
                    edges.Remove(edges[i]);
            }
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].to== Int32.Parse(id) || links[i].from== Int32.Parse(id))
                {
                    links.Remove(links[i]);
                    i--;
                }
            }
        }

        public void rmLink(string name)
        {
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].name==name)
                    links.Remove(links[i]);
            }
        }

        public void Clear()
        {
            edges.Clear();
            links.Clear();
        }

        public int[] getEdges()
        {
            int[] strs = new int[edges.Count];
            for (int i = 0; i < edges.Count; i++)
            {
                strs[i] = edges[i].id;
            }
            return strs;
        }

        public string[] getEdgesNames()
        {
            var strs = new string[edges.Count];
            for (int i = 0; i < edges.Count; i++)
            {
                strs[i] = edges[i].id+"";
            }
            return strs;
        }

        public string[] getLinks()
        {
            string[] strs = new string[links.Count];
            for (int i = 0; i < links.Count; i++)
            {
                strs[i] = links[i].name;
            }
            return strs;
        }

        public int getEdgeID(int s)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                if (edges[i].id == s)
                    return i;
            }
            return -1;
        }
        public Edge getEdge(int id)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                if (edges[i].id == id)
                    return edges[i];
            }
            return null;
        }
    }
}
