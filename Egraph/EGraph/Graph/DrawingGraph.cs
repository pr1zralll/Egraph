using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Egraph.Forms.gPanel;

namespace Egraph
{
    public abstract class DrawingGraph:BaseGraph
    {
        public DrawingGraph() : base()
        {
        }

        public void onLoad(string path)
        {
            load(path);
        }


        public void onLoad(FileFormat format)
        {
            //"System drawingGraph (*.sg)|*.sg"
            //"Task drawingGraph (*.tg)|*.tg";
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = format == FileFormat.SystemGraph ? "System drawingGraph (*.sg)|*.sg" : "Task drawingGraph (*.tg)|*.tg";
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
                load(dialog.FileName);
        }

        public void onSave(FileFormat format)
        {
            //"System drawingGraph (*.sg)|*.sg"
            //"Task drawingGraph (*.tg)|*.tg";
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = format == FileFormat.SystemGraph ? "System drawingGraph (*.sg)|*.sg" : "Task drawingGraph (*.tg)|*.tg";
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
                save(dialog.FileName);
        }
        public void save(string fileName)
        {
            File.Create(fileName);
            using (Stream stream = File.Open(fileName + "e", FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, edges);
            }
            using (Stream stream = File.Open(fileName + "l", FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, links);
            }
        }

        public void load(string fileName)
        {
            using (Stream stream = File.Open(fileName + "e", FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                edges = (List<Edge>) bformatter.Deserialize(stream);
            }
            using (Stream stream = File.Open(fileName + "l", FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                links = (List<Link>) bformatter.Deserialize(stream);
            }
        }
        
        public void gRmLink()
        {
            FormRmLink formRmLink = new FormRmLink(this);
            formRmLink.ShowDialog();
        }

        public void gRmEdge()
        {
            FormRmEdge formRmEdge = new FormRmEdge(this);
            formRmEdge.ShowDialog();
        }

        public void gAddLink()
        {
            FormAddLink formAddLink = new FormAddLink(this);
            formAddLink.ShowDialog();
        }

        public void gAddEdge()
        {
            FormAddEdge formAddEdge = new FormAddEdge(this);
            formAddEdge.ShowDialog();
        }

        public void drawLine(Graphics g, int x1, int y1, int x2, int y2, string name)
        {
            int xc = (x2 + x1) / 2;
            int yc = (y2 + y1) / 2;
            g.DrawLine(Pens.Black, x1, y1, x2, y2);
            g.DrawString(name, new Font("Consolas", 11), Brushes.MidnightBlue, xc - 10, yc - 10);
        }

        public void drawArrow(Graphics g, int x1, int y1, int x2, int y2, string name)
        {
            int xs = ((x2 + x1) / 2+x2)/2;
            int ys = ((y2 + y1) / 2+y2)/2;
            Pen pen = new Pen(Color.Black, 2);

            //**********************
            GraphicsPath hPath = new GraphicsPath();
            hPath.AddPolygon(new Point[]{new Point(-5, 0), new Point(5, 0), new Point(0, 5)});
          
            CustomLineCap HookCap = new CustomLineCap(null, hPath);
            Pen customCapPen = new Pen(Color.Black, 1);
            customCapPen.CustomEndCap = HookCap;
            
            g.DrawLine(customCapPen,x1,y1,x2,y2);

            //*******************************************
            g.DrawLine(Pens.Black, x1, y1, x2, y2);
            g.FillRectangle(Brushes.White,xs-10,ys-10,30,15);
            g.DrawRectangle(Pens.Black, xs - 10, ys - 10, 30, 15);
            g.DrawString(name, new Font("Consolas", 11), Brushes.MidnightBlue, xs - 10, ys - 10);
        }

        protected void drawEdgeCircle(Graphics g, int x, int y, Edge edge)
        {
            Font font = new Font("System", 9);
            int size = 20;
            g.FillEllipse(Brushes.White, x - size / 2, y - size / 2, size, size);
            g.DrawString(edge.id+"", font, Brushes.Black, x - 7, y -9);
            g.FillRectangle(Brushes.White,x+12,y-8,20,15);
            g.DrawString(edge.weight + "", font, Brushes.Red, x + 12, y - 10);
            g.DrawLine(Pens.Black, x + 10, y + 5, x + 30, y + 5);
            
            g.DrawEllipse(Pens.Black, x - size / 2, y - size / 2, size, size);
            edge.position = new Point(x, y);
        }
        protected void drawEdgeSquad(Graphics g, int x, int y, Edge edge)
        {
            Font font = new Font("Consolas", 11);
            int size = 30;
            g.FillRectangle(Brushes.White, x - size / 2, y - size / 2, size, size);
            g.DrawString(""+edge.id, font, Brushes.Black, x - 10, y - 10);
            g.DrawString(""+edge.weight, font, Brushes.Crimson, x + 20, y - 10);
            g.DrawLine(Pens.Black, x + 20, y + 5, x + 40, y + 5);

            g.DrawRectangle(Pens.Black, x - size / 2, y - size / 2, size, size);
            edge.position = new Point(x, y);
        }



        public enum FileFormat
        {
            SystemGraph,TaskGraph
        }

        public abstract Bitmap drawGraph();
    }
}