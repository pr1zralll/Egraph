﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egraph
{
    [Serializable]
    public class Link
    {
        public Link(string weight, string from, string to)
        {
            this.weight = Int32.Parse(weight);
            this.from = Int32.Parse(from);
            this.to = Int32.Parse(to);
            this.name = from+"->"+to;
        }
       
        public string name;
        public int weight;
        public int from;
        public int to;

        public Link(int w, int from, int to)
        {
            this.weight = w;
            this.from = from;
            this.to = to;
            this.name = from + "->" + to;
        }

        public void Revert()
        {
            int temp = from;
            from = to;
            to = temp;
        }
    }
}
