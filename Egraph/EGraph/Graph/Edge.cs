﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Egraph
{
    [Serializable]
    public class Edge
    {
        public Edge(string name, string weight)
        {
            this.id = Int32.Parse(name);
            this.weight = Int32.Parse(weight);
        }
        public int id;
        public int weight;
        public Point position;
    }
}
