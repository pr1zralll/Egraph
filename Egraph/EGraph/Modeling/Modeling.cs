﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph
{
    public class Modeling
    {
        public SystemGraph s_graph;
        public TaskGraph t_gtaph;

        public Modeling(TaskGraph taskGraph, SystemGraph systemGraph)
        {
            this.t_gtaph = taskGraph;
            this.s_graph = systemGraph;
        }

        public void queue_3()
        {
            new Queue3(this).create();
        }
        public void queue_9()
        {
            new Queue9(this).create();
        }
        public void queue_14()
        {
            new Queue14(this).create();
        }

        public void process_2()
        {
            new Process2(this).create();
        }
        public void process_5()
        {
            new Process5(this).create();
        }
    }
}
