using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Egraph
{
    public class Queue14:QueueBase
    {
        public Queue14(Modeling modeling) : base(modeling)
        {
            
        }
        
        //� ������� �������� ���� ������
        //���� ������
        public override void create()
        {
            t_gtaph.edges = new List<Edge>(t_gtaph.edges.OrderByDescending(o => o.weight));
            showQueue14(t_gtaph.edges);
        }

        private void showQueue14(List<Edge> e)
        {
            var str = new StringBuilder();
            for (int i = 0; i < e.Count; i++)
            {
                str.Append(e[i].id + "(" + e[i].weight + ")>");
            }
            MessageBox.Show("� ������� �������� ���� ������ \n(���� ������)\n" + str);
        }
    }
}