using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Egraph
{
    public class Queue3: QueueBase
    {
        public Queue3(Modeling modeling) : base(modeling)
        {
            
        }

        //� ������� �������� ���������� �� ���� ������ �� ���� ����� ������.
        //��������� ���� �� ���� ��� ��� ������
        public override void create()
        {
            var allWays = getAllWays(t_gtaph);
            var wayscount = calcWayscount(allWays);
            var ways = FindCriticalWaysTime(wayscount, allWays);

            var sum = new List<int>();
            for (int i = 0; i < ways.Count; i++)
            {
                sum.Add(calcWayTime(ways[i]));
            }
            //
            var list = new List<KeyValuePair<Edge, int>>();
            for (int i = 0; i < sum.Count; i++)
            {
                list.Add(new KeyValuePair<Edge, int>(t_gtaph.edges[i], sum[i]));
            }
            list = new List<KeyValuePair<Edge, int>>(list.OrderByDescending(o => o.Value));
            var rez = new List<Edge>();
            for (int i = 0; i < list.Count; i++)
            {
                rez.Add(list[i].Key);
            }

            t_gtaph.edges = rez;

            showQueue3(rez, list);
        }

   


        private void showQueue3(List<Edge> rez, List<KeyValuePair<Edge, int>> list)
        {
            var str = new StringBuilder();
            for (int i = 0; i < rez.Count; i++)
            {
                str.Append(rez[i].id + "(" + list[i].Value + ")" + ">");
            }
            MessageBox.Show("� ������� �������� ���������� �� ���� ������ �� ���� ����� ������. \n(��������� ���� �� ���� ��� ��� ������)\n" + str.ToString());
        }
    }
}