﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph
{
    class Queue9:QueueBase
    {
        public Queue9(Modeling modeling) : base(modeling)
        {
        }


        // У порядку зростання по кількості вершин шляхів ДО початку графа задач.
        //    Критичний шлях по кількості вершин
        public override void create()
        {
            t_gtaph.RevertLinks();
            var allWays = getAllWays(t_gtaph);
            t_gtaph.RevertLinks();
            var wayscount = calcWayscount(allWays);
            var ways = FindCriticalWaysCount(wayscount, allWays);

            ways = new List<List<Edge>>(ways.OrderBy(o => o.Count));
           
            var rez = new List<Edge>();

            for (int i = 0; i < ways.Count; i++)
            {
                rez.Add(ways[i][0]);
            }
            
            t_gtaph.edges = rez;

            showQueue9(ways);
        }

        private void showQueue9(List<List<Edge>> allWay)
        {
            var str = new StringBuilder();
            for (int i = 0; i < allWay.Count; i++)
            {
                str.Append(allWay[i][0].id + "(" + allWay[i].Count + ")" + ">");
            }
            MessageBox.Show("У порядку зростання по кількості вершин шляхів ДО початку графа задач \n(Критичний шлях по кількості вершин)\n" + str.ToString());
        }
    }
}
