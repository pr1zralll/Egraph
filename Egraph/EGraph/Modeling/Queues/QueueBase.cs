using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Egraph
{
    public abstract class QueueBase
    {
        protected SystemGraph s_graph;
        protected TaskGraph t_gtaph;

        public QueueBase(Modeling modeling)
        {
            this.s_graph = modeling.s_graph;
            this.t_gtaph = modeling.t_gtaph;
        }

        public abstract void create();

        public int calcWayTime(List<Edge> edges)
        {
            int sum = 0;
            for (int i = 0; i < edges.Count; i++)
            {
                sum += edges[i].weight;
            }
            return sum;
        }
        
        public List<List<Edge>> FindCriticalWaysTime(int[] wayscount, List<List<Edge>> allWays)
        {
            var ways = new List<List<Edge>>();
            //find max way from ways>1
            int pointer = 0;
            for (int i = 0; i < wayscount.Length; i++)
            {
                if (wayscount[i] > 1)
                {
                    int max_i = 0;
                    int max = 0;
                    for (int j = 0; j < wayscount[i]; j++)
                    {
                        int curr = calcWayTime(allWays[pointer + j]);
                        if (curr > max)
                        {
                            max = curr;
                            max_i = pointer + j;
                        }
                    }
                    ways.Add(allWays[max_i]);
                    pointer += wayscount[i];
                }
                else
                {
                    ways.Add(allWays[pointer++]);
                }
            }
            return ways;
        }
        public List<List<Edge>> FindCriticalWaysCount(int[] wayscount, List<List<Edge>> allWays)
        {
            var ways = new List<List<Edge>>();
            //find max way from ways>1
            int pointer = 0;
            for (int i = 0; i < wayscount.Length; i++)
            {
                if (wayscount[i] > 1)
                {
                    int max_i = 0;
                    int max = 0;
                    for (int j = 0; j < wayscount[i]; j++)
                    {
                        int curr = allWays[pointer + j].Count;
                        if (curr > max)
                        {
                            max = curr;
                            max_i = pointer + j;
                        }
                    }
                    ways.Add(allWays[max_i]);
                    pointer += wayscount[i];
                }
                else
                {
                    ways.Add(allWays[pointer++]);
                }
            }
            return ways;
        }
        public int[] calcWayscount(List<List<Edge>> allWays)
        {
            var wayscount = new int[t_gtaph.edges.Count];

            for (int j = 0; j < allWays.Count; j++)
            {
                wayscount[t_gtaph.getEdgeID(allWays[j][0].id)]++;
            }
            return wayscount;
        }

        public List<List<Edge>> getAllWays(TaskGraph tGtaph)
        {
            var ways = new List<List<Edge>>();
            var startPoints = getStartPoints(tGtaph);
            var endPoints = getEndPoints(tGtaph);
            for (int i = 0; i < startPoints.Count; i++)
            {
                for (int j = 0; j < endPoints.Count; j++)
                    ways.AddRange(findWay(tGtaph, startPoints[i], endPoints[j]));
            }
            return ways;
        }
        
        private List<List<Edge>> findWay(TaskGraph tGtaph, Edge startPoint, Edge endPoint)
        {
            var links = tGtaph.links;
            var edges = tGtaph.edges;
            var ways = new List<List<Edge>>();

            if (startPoint.id == endPoint.id)
            {
                var way = new List<Edge>();
                way.Add(startPoint);
                ways.Add(way);
                return ways;
            }

            FindRoute(edges, links, ways, startPoint, endPoint);
            used = null;
            return ways;
        }

        private Boolean[] used = null;
        private List<Edge> route = null;

        private void FindRoute(List<Edge> edges, List<Link> links, List<List<Edge>> list, Edge v, Edge endPoint)
        {
            if (used == null)
            {
                used = new Boolean[edges.Count];
                route = new List<Edge>();
                used.Initialize();
            }
            route.Add(v);
            if (v.id == endPoint.id)
            {
                var temp = new List<Edge>();
                temp.AddRange(route);
                list.Add(temp);
            }
            else
            {
                for (int j = 0; j < links.Count; j++)
                {
                    if (v.id == links[j].from)
                    {
                        var i = t_gtaph.getEdgeID(links[j].to);
                        var w = edges[i];
                        //��� ���� ������� � V ������ (W)
                        if (!used[i])
                        {
                            used[i] = true;
                            FindRoute(edges, links, list, w, endPoint);
                            used[i] = false;
                            route.Remove(w);
                        }
                    }
                }
            }
        }

        private List<Edge> getEndPoints(TaskGraph tGtaph)
        {
            var points = new List<Edge>();
            for (int i = 0; i < tGtaph.edges.Count; i++)
            {
                bool found = false;

                for (int j = 0; j < tGtaph.links.Count; j++)
                {
                    if (tGtaph.edges[i].id==tGtaph.links[j].from)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    points.Add(tGtaph.edges[i]);
                }
            }
            return points;
        }

        private List<Edge> getStartPoints(TaskGraph tGtaph)
        {
            var list = new List<Edge>();
            list.AddRange(tGtaph.edges);
            return list;
        }
    }
}