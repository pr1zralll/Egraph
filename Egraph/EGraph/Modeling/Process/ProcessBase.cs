﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egraph
{
    abstract class ProcessBase
    {
        public SystemGraph s_graph;
        public TaskGraph t_graph;

        protected ProcessBase(Modeling modeling)
        {
            this.t_graph = modeling.t_gtaph;
            this.s_graph = modeling.s_graph;
        }

        abstract public void create();

        private Boolean[] used = null;
        private List<List<Edge>> list = null;
        private List<Edge> route = null;
        private List<List<Edge>> FindRoute(Edge v, Edge endPoint)
        {
            route.Add(v);
            if (v.id == endPoint.id)
            {
                List<Edge> temp = new List<Edge>();
                temp.AddRange(route);
                list.Add(temp);
            }
            else
            {
                for (int j = 0; j < s_graph.links.Count; j++)
                {
                    if (v.id == s_graph.links[j].from)
                    {
                        var i = s_graph.getEdgeID(s_graph.links[j].to);
                        var w = s_graph.edges[i];
                        if (!used[i])
                        {
                            used[i] = true;
                            FindRoute(w, endPoint);
                            used[i] = false;
                            route.Remove(w);
                        }
                    }
                    else if (v.id == s_graph.links[j].to)
                    {
                        var i = s_graph.getEdgeID(s_graph.links[j].from);
                        var w = s_graph.edges[i];
                        if (!used[i])
                        {
                            used[i] = true;
                            FindRoute(w, endPoint);
                            used[i] = false;
                            route.Remove(w);
                        }
                    }
                }
            }
            return list;
        }

        public List<List<Edge>> getRoute(int start, int end)
        {
            list = new List<List<Edge>>();
            used = new Boolean[s_graph.edges.Count];
            route = new List<Edge>();
            used.Initialize();
            return FindRoute(s_graph.getEdge(start), s_graph.getEdge(end));
        }
    }
 
}
