﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Egraph
{
    class Process2:ProcessBase
    {

        public Process2(Modeling modeling) : base(modeling)
        {
        }

        public override void create()
        {
            var queue = createTaskQueue();
        }

        private Queue<Edge> createTaskQueue()
        {
            var edges = new Queue<Edge>();
            for (int i = 0; i < t_graph.edges.Count; i++)
            {
                edges.Enqueue(t_graph.edges[i]);
            }

            var links = new List<Link>();
            links.AddRange(t_graph.links);

            Queue<Edge> queue = new Queue<Edge>();

            while (edges.Count > 0)
            {
               
                    var edge = edges.Peek();

                    var found = false;

                    for (int j = 0; j < links.Count; j++)
                    {
                        var link = links[j];

                        if (link.to == edge.id)
                            found = true;
                    }
                if (!found)
                {
                    queue.Enqueue(edges.Dequeue());

                    for (int j = 0; j < links.Count; j++)
                    {
                        var link = links[j];

                        if (link.from == edge.id)
                            links.Remove(link);
                    }
                }
                else
                {
                    edges.Enqueue(edges.Dequeue());
                }
                
            }
            return queue;
        }
    }
}
