﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Egraph.Forms;

namespace Egraph
{
    public class SystemGraph:DrawingGraph
    {
        public SystemGraph() : base()
        {
        }

        public override Bitmap drawGraph()
        {
            check();
            Bitmap image = new Bitmap(500,500);
            if (edges.Count < 1) return image;

            int width = image.Width;
            int height = image.Height;
            var g = Graphics.FromImage(image);
            
            //location
            int ii = 0;
            for (int a = 0; a < 360; a+=360/edges.Count)
            {
                int x = (int) (height / 3 * Math.Cos(a* 0.0174533)+width/2);
                int y = (int)(height / 3 * Math.Sin(a* 0.0174533)+height/2);
                if(ii<edges.Count)
                    edges[ii++].position = new Point(x,y);
            }
            //draw links
            for (int i = 0; i < links.Count; i++)
            {
                int x1 = edges[getEdgeID(links[i].from)].position.X;
                int y1 = edges[getEdgeID(links[i].from)].position.Y;
                int x2 = edges[getEdgeID(links[i].to)].position.X;
                int y2 = edges[getEdgeID(links[i].to)].position.Y;

                drawLine(g, x1, y1, x2, y2, links[i].weight+"");
            }
            //draw 
            for (int i = 0; i < edges.Count; i++)
            {
                drawEdgeSquad(g,edges[i].position.X, edges[i].position.Y,edges[i]);
            }
            return image;
        }
        
        private void check()
        {
            bool exists = false;
            for (int i = 1; i < edges.Count; i++)
            {
                if (!RouteExists(edges[0], edges[i]))
                {
                    exists = false;
                    break;
                }
                exists = true;
            }
            if(exists)
                 Form1.stateBox.Checked = true;
            else
                 Form1.stateBox.Checked = false;
        }
        Boolean[] used = null;
        List<List<Edge>> list = null;
        private List<Edge> route = null;
        private List<List<Edge>> FindRoute(Edge v, Edge endPoint)
        {
          
            route.Add(v);
            if (v.id == endPoint.id)
            {
                List<Edge> temp = new List<Edge>();
                temp.AddRange(route);
                list.Add(temp);
            }
            else
            {
                for (int j = 0; j < links.Count; j++)
                {
                    if (v.id == links[j].from)
                    {
                        var i = getEdgeID(links[j].to);
                        var w = edges[i];
                        //для всех смежных с V вершин (W)
                        if (!used[i])
                        {
                            used[i] = true;
                            FindRoute(w, endPoint);
                            used[i] = false;
                            route.Remove(w);
                        }
                    }
                    else if (v.id == links[j].to)
                    {
                        var i = getEdgeID(links[j].from);
                        var w = edges[i];
                        //для всех смежных с V вершин (W)
                        if (!used[i])
                        {
                            used[i] = true;
                            FindRoute(w, endPoint);
                            used[i] = false;
                            route.Remove(w);
                        }
                    }
                }
            }
            return list;
        }

        private bool RouteExists(Edge start, Edge end)
        {
            list = new List<List<Edge>>();
            used = new Boolean[edges.Count];
            route = new List<Edge>();
            used.Initialize();
            return FindRoute(start, end).Count>0;
        }

    }
}
