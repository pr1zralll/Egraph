﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Egraph.Forms;
using Egraph.Forms.gPanel;

namespace Egraph
{

    public class TaskGraph : DrawingGraph
    {
        public TaskGraph():base()
        {
        }
        public override Bitmap drawGraph()
        {
            Bitmap image = new Bitmap(500,500);
            int width = image.Width;
            int height = image.Height;
            if (edges.Count < 1) return image;
            var g = Graphics.FromImage(image);

            //locate edges

            int ii = 0;
            for (int a = 0; a < 360; a += 360 / edges.Count)
            {
                int x = (int)(height / 3 * Math.Cos(a * 0.0174533) + width / 2);
                int y = (int)(height / 3 * Math.Sin(a * 0.0174533) + height / 2);
                if (ii < edges.Count)
                    edges[ii++].position = new Point(x, y);
            }


            //draw links
            for (int i = 0; i < links.Count; i++)
            {
                int x1 = edges[getEdgeID(links[i].from)].position.X;
                int y1 = edges[getEdgeID(links[i].from)].position.Y; 
                int x2 = edges[getEdgeID(links[i].to)].position.X;
                int y2 = edges[getEdgeID(links[i].to)].position.Y;
          
                drawArrow(g, ((((x1+x2)/2+x1)/2+x1)/2+x1)/2,(((( y1+y2)/2+y1)/2+y1)/2+y1)/2, (((x2+x1)/2+x2)/2+x2)/2, (((y2+y1)/2+y2)/2+y2)/2, links[i].weight+"");
            }
            //draw 
            for (int i = 0; i < edges.Count; i++)
            {
                drawEdgeCircle(g, edges[i].position.X, edges[i].position.Y, edges[i]);
            }
            Form1.corr.Text ="links: "+links.Count+"  edges: "+edges.Count + "  corr: " + correlation() ;
            return image;
        }


        public void gGen(int minWeight, int maxWeight, int edgeCount, float corr, int minLink, int maxLink)
        {
            Clear();
            Random r = new Random();
            for (int i = 0; i < edgeCount; i++)
            {
                addEdge(new Edge((i + 1)+"",""+ (r.Next(maxWeight - minWeight) + minWeight)));
            }
            double cor = 1;
            while (Math.Abs(cor - corr)>=0.01)
            {
                for (int i = 1; i < edgeCount; i++)
                {
                    int l = minLink;
                    int from = i;
                    int to = r.Next(edgeCount - i) + i + 1;
                    addLinkSum(new Link(l,from, to));
                    cor = correlation();
                    if(Math.Abs(cor - corr) <= 0.01)
                        return;
                    if (cor < corr)
                        return;
                }
            }
        }

        public double correlation()
        {
            double w = 0;
            for (int i = 0; i < edges.Count; i++)
            {
                w += edges[i].weight;
            }
            double l = 0;
            for (int i = 0; i < links.Count; i++)
            {
                l += links[i].weight;
            }
            return w / (w + l);
        }

      
    }
}
